var isUserNameSet = false;
var isConnectionLost = false;
var isAlertAvaliable = true;
var userName;
var ws;
var encryptor;
var DEBUG = false;

$(document).ready(function() {
    checkForLogin(); //preverimo ali je uporabniško ime shranjeno v localstorage

    encryptor = new Encryptor();
    encryptor.init();

    initWebSocket(); //inicializiramo web socket

    initControlEvents(); //init button and other control events

    if (localStorage.isAlertAvaliable != undefined) {
        isAlertAvaliable = localStorage.isAlertAvaliable == 'true';
    }

    setAlertIcons();

    initSounds();
});

function initSounds() {

    // init bunch of sounds
    ion.sound({
        sounds: [
            { name: "button_tiny" }
        ],

        // main config
        path: "sounds/",
        preload: true,
        multiplay: true,
        volume: 0.9
    });
}

function initControlEvents() {
    $('#loginButton').click(login);

    $('#userNameInput').keypress(function(e) {
        if (e.keyCode == 13)
            login();
    });

    $('#sendChatButton').click(sendChat);

    $('#sendInputText').keypress(function(e) {
        if (e.keyCode == 13) {
            e.preventDefault();
            sendChat();
        }
    });

    $('#alertToggleButton').click(function(e) {
        e.preventDefault();

        if (isAlertAvaliable) {
            isAlertAvaliable = false;
        } else {
            isAlertAvaliable = true;
        }

        localStorage.isAlertAvaliable = isAlertAvaliable;

        setAlertIcons();
    });

    $('#signOutButton').click(function(e) {
        e.preventDefault();
        signOut();
    });

}

function setAlertIcons() {
    if (isAlertAvaliable == false) {
        $('#alertToggleButton').removeClass('fa-bell-slash');
        $('#alertToggleButton').addClass('fa-bell');
    } else {
        $('#alertToggleButton').removeClass('fa-bell');
        $('#alertToggleButton').addClass('fa-bell-slash');
    }
}

function signOut() {
    localStorage.removeItem('userName');
    location.reload();
}

//login funkcija
function login(defaultUsername) {
    userName = $('#userNameInput').val();
    if (userName && userName.length > 3) {
        var sendingObject = {
            type: 'login',
            data: {
                userName: userName
            }
        };
        var sendingObjectJSON = JSON.stringify(sendingObject);
        ws.send(sendingObjectJSON);
    }
}

//preverimo ali je uporabniško ime shranjeno v localstorage in nastavimo kontrole če je
function checkForLogin() {
    var userNameCheck = localStorage.userName;
    if (userNameCheck != undefined) {
        isUserNameSet = true;
        userName = userNameCheck;
        $('#userNameInput').val(userName);
    }
}

//init websocket
function initWebSocket() {
    ws = new WebSocket('ws://klemen.kainoto.com:4080');

    //on open preverimo če je username set, in če je potem se avtomatsko prijavimo
    ws.onopen = function() {
        if (isUserNameSet) { // && isConnectionLost
            isConnectionLost = false;
            var sendingObject = {
                type: 'login',
                data: {
                    userName: userName
                }
            };
            var sendingObjectJSON = JSON.stringify(sendingObject);
            ws.send(sendingObjectJSON);
        }
    };

    //callback ko dobimo iz strežnika podatke
    ws.onmessage = function(evt) {
        var receivedMsg = evt.data;
        //console.log("Message is received...", receivedMsg);

        var dataFromServer = JSON.parse(receivedMsg);
        var type = dataFromServer.type;
        var data = dataFromServer.data;

        console.log(type);
        console.log(data);

        switch (type) {
            case 'connected':
                connectedToWebSocketServer(data.message);
                break;
            case 'debug':
                //console.log('data from client: ', dataFromServer.data.debugData);
                break;
            case 'login':
                loginCallBack(data);
                break;
            case 'getUsers':
                renderUsers(data);
                break;
            case 'chat':
                chatReceived(data);
                break;
            case 'publicKeyRequest':
                requestForPublicKeyReceived(data);
                break;
            case 'sendPublicKey':
                getPublicKeyFromUser(data);
                break;
            case 'generateNewKey':
                generateNewKey();
                break;
        }
    };

    ws.onclose = function() {
        console.log("Connection is closed...");
        showMessageBox('Povezava s strežnikom izgubljena', false, 3000);
        if (isUserNameSet) {
            isConnectionLost = true;
            initWebSocket();
        }
    };
}

function generateNewKey() {
    console.log('generate new key');
    localStorage.removeItem('passPhrase');
    encryptor.publicKeysStorage = [];
    encryptor.init();
}

function requestForPublicKeyReceived(data) {
    console.log('requestForPublicKeyReceived', data);
    var myPublicKey = encryptor.publicKey;
    var sender = data.sender;
    var keyOutput = {
        type: 'sendPublicKey',
        data: {
            receiver: sender,
            publicKey: myPublicKey
        }
    };

    ws.send(JSON.stringify(keyOutput));
}

function getPublicKeyFromUser(data) {
    console.log('getPublicKeyFromUser', data);
    encryptor.publicKeysStorage[data.sender] = data.publicKey;
    encryptor.onGetKeyPublic(data.publicKey);
}

function chatReceived(data) {
    console.log('received chat:', data);
    var sender = data.sender;
    var msg = encryptor.decryptTextFromSender(data.message);
    var needToCreateCircle = false;
    var chatView = createChatView(msg, 'received');

    var chatWindowExists = $('.chatWindow[data-user-name="' + sender + '"]').length == 1;
    if (chatWindowExists) {
        $('.chatWindow[data-user-name="' + sender + '"]').append(chatView);
        if ($('.chatWindow[data-user-name="' + sender + '"]').hasClass('active')) {
            scrollToBottom($('.chatWindow.active'));
        } else {
            needToCreateCircle = true;
        }
    } else {
        //kreiramo novi chat window
        var $newChatWindow = $('<div></div>', {
            class: 'chatWindow',
            'data-user-name': sender
        });
        $newChatWindow.append(chatView);
        $('#chatWrapper').prepend($newChatWindow);
        needToCreateCircle = true;
    }

    if (needToCreateCircle) {
        createMessageCircleAlert(sender);
    }

    alertChatReceived();
}

function alertChatReceived() {
    if (isAlertAvaliable) {
        // play alert sound
        ion.sound.play("button_tiny");
    }
}

function createMessageCircleAlert(userName) {
    var $userDiv = $('.userDiv[data-user-name="' + userName + '"]');
    var $messageCircle = $userDiv.find('.messageCircle');
    var currentValue = 0;

    if ($messageCircle.length == 1) {
        currentValue = parseInt($messageCircle.html().trim());
    } else {
        $messageCircle = $('<div></div>', {
            class: 'messageCircle'
        });
        $userDiv.append($messageCircle);
    }

    currentValue += 1;
    $messageCircle.html(currentValue);
}

function renderUsers(data) {
    $('#initialLoader').remove();
    $('#userList, #chatWrapper').removeClass('hidden');

    var users = data.users;
    var indexOfUserName = users.indexOf(userName);
    console.log(users);
    console.log(indexOfUserName);
    if (indexOfUserName > -1) {
        users.splice(indexOfUserName, 1);
        console.log(users);
    }


    var usersToAdd = [];
    users.forEach(function(user) {
        if ($('.userDiv[data-user-name="' + user + '"]').length == 0) {
            usersToAdd.push(user);
        }
    }, this);

    $('.userDiv').each(function() {
        var userNameTemp = $(this).attr('data-user-name');
        if (users.indexOf(userNameTemp) == -1) {
            $(this).remove();
        }
    });

    usersToAdd.forEach(function(user) {
        $('#userList').append(createUserDiv(user));
    }, this);
    console.log('render on screen', users);
}

function createUserDiv(user) {
    var $userDiv = $('<div></div>', {
        class: 'userDiv',
        text: user,
        'data-user-name': user,
    });

    $userDiv.click(openChatWindow);

    $userDiv.attr('data-user-name', user);
    return $userDiv;
}

function openChatWindow() {
    $('.userDiv').removeClass('active');
    $(this).addClass('active');
    var userName = $(this).attr('data-user-name');
    $(this).find('.messageCircle').remove();
    console.log('open windows to chat with', userName);

    var chatWindowExists = $('.chatWindow[data-user-name="' + userName + '"]').length == 1;
    if (chatWindowExists) {
        $('.chatWindow').removeClass('active');
        $('.chatWindow[data-user-name="' + userName + '"]').addClass('active');
    } else {
        //kreiramo novi chat window
        var $newChatWindow = $('<div></div>', {
            class: 'chatWindow active',
            'data-user-name': userName
        });

        $('.chatWindow').removeClass('active');
        $('#chatWrapper').prepend($newChatWindow);

    }

    scrollToBottom($('.chatWindow.active'));
}

function loginCallBack(loginData) {
    var isLoginOk = loginData.logged;
    var message = loginData.message;

    showMessageBox(message, isLoginOk);

    if (isLoginOk) {
        console.log(userName);
        localStorage.userName = userName;
        $('#loginOverlay').hide();
        setTimeout(getUsers, 0 /*1000*/ );

        setUsernameView(userName);
    }
}

function setUsernameView(userName) {
    $('#userNameSpan').html(userName);
}

String.prototype.replaceAll = function(search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
};

function replaceNotWorkingChars(message) {
    message = message.replaceAll('č', 'c');
    message = message.replaceAll('š', 's');
    message = message.replaceAll('ž', 'z');
    message = message.replaceAll('Č', 'C');
    message = message.replaceAll('Š', 'S');
    message = message.replaceAll('Ž', 'Z');
    return message;
}

function sendChat(receiver) {
    var message = $('#sendInputText').val();
    message = replaceNotWorkingChars(message);
    if (message && message.length > 0) {
        var receiver = $('.chatWindow.active').attr('data-user-name');
        if (receiver != undefined) {
            encryptor.getReceiverPublicKey(receiver, function(receiverPublicKey) {
                encryptor.onGetKeyPublic = null;
                var encryptedMessage = encryptor.encryptTextForSending(message, receiverPublicKey);
                var chatOutput = {
                    type: 'chat',
                    data: {
                        receiver: receiver,
                        message: encryptedMessage
                    }
                };

                $('#sendInputText').val('');
                $('.chatWindow.active').append(createChatView(message, 'sent'));
                scrollToBottom($('.chatWindow.active'));

                ws.send(JSON.stringify(chatOutput));
            });

        } else {
            $('#sendInputText').val('');
            showMessageBox('Najprej izberi nekoga za klepet', false, 5000);
        }
    }
}

function scrollToBottom(element) {
    element.scrollTop(element.prop("scrollHeight"));
}

function createChatView(msg, className) {
    var $sentChatView = $('<div></div>', {
        class: 'chatBox ' + className,
        text: msg
    });

    var dateStringFormat = moment().format('HH:mm');
    var $dateView = $('<span></span>', {
        class: 'chatBoxDate',
        html: dateStringFormat
    });

    $sentChatView.append($dateView);

    return $sentChatView;
}

function getUsers() {
    var getUserData = {
        type: 'getUsers'
    };
    ws.send(JSON.stringify(getUserData));
}

function showMessageBox(msg, ok, timeout) {
    if (ok == undefined) ok = true;
    if (timeout == undefined) timeout = 5000;

    var className = ok ? '' : 'error'
    var iconClassName = ok ? 'fa-info-circle' : 'fa-exclamation-triangle';
    var icon = '<i class="fa ' + iconClassName + '"></i> '

    var $messageBox = $('<div></div>', {
        class: 'messageBox ' + className,
        html: icon + msg
    });

    var $closeButton = $('<div></div>', {
        class: 'closeMessageBoxBtn',
        text: '✖',
        title: 'Zapri'
    });

    var removeTimeout = setTimeout(function() {
        $messageBox.remove();
    }, timeout);

    $closeButton.click(function() {
        $messageBox.remove();
        clearTimeout(removeTimeout);
    });

    $messageBox.append($closeButton);
    $('#messageBoxContainer').append($messageBox);
}

function connectedToWebSocketServer(message) {
    console.log(message);
    showMessageBox(message);
}