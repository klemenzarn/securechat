//https://github.com/wwwtyro/cryptico
function Encryptor() {
    this.privateKey;
    this.publicKey;
    this.publicKeysStorage = [];
    this.onGetKeyPublic = null;
}

//generiranje privatnih in javnih klučev
Encryptor.prototype.init = function() {
    this.generateMyPrivateKey();
    this.generateMyPublicKey();
};

Encryptor.prototype.generateMyPublicKey = function() {
    this.publicKey = cryptico.publicKeyString(this.privateKey);
    console.log('publicKey', this.publicKey);
};

Encryptor.prototype.generatePassPhrase = function() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < 60; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}

Encryptor.prototype.generateMyPrivateKey = function() {
    var passPhrase = localStorage.passPhrase;
    if (passPhrase == undefined) {
        passPhrase = this.generatePassPhrase();
        localStorage.passPhrase = passPhrase;
    }

    var bits = 1024;
    console.log(passPhrase);
    this.privateKey = cryptico.generateRSAKey(passPhrase, bits);

    console.log('private key', this.privateKey);
};

//enkripcija teksta pred pošiljanjem
Encryptor.prototype.encryptTextForSending = function(text, receiverPublicKey) {
    var encryptionResult = cryptico.encrypt(text, receiverPublicKey, this.privateKey);
    console.log(encryptionResult);
    return encryptionResult.cipher;
};

//dekripcija teksta pri prejemanju sporočila
Encryptor.prototype.decryptTextFromSender = function(encryptedText) {
    var decryptionResult = cryptico.decrypt(encryptedText, this.privateKey);
    console.log(decryptionResult);
    return decryptionResult.plaintext;
};

//pridobivanje public key-a za enkripcijo teksta, če ga ni ga moramo zahtevat od
//prejemnika sporočila
Encryptor.prototype.getReceiverPublicKey = function(receiver, onGetKey) {
    if (this.publicKeysStorage[receiver] == undefined) {
        console.log('handshaking public keys');
        this.onGetKeyPublic = onGetKey;
        var chatOutput = {
            type: 'publicKeyRequest',
            data: {
                receiver: receiver
            }
        };

        ws.send(JSON.stringify(chatOutput));
    } else {
        onGetKey(this.publicKeysStorage[receiver]);
    }
};