var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var chat = require('./routes/chat.js');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', chat);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

// ws server init
var WSserver = require('http').createServer();
var url = require('url');
var WebSocketServer = require('ws').Server;
var wss = new WebSocketServer({ server: WSserver });
var port = 4080;

wss.on('connection', function connection(ws) {
    var location = url.parse(ws.upgradeReq.url, true);
    // you might use location.query.access_token to authenticate or share sessions
    // or ws.upgradeReq.headers.cookie (see http://stackoverflow.com/a/16395220/151312)
    // wss.clients.forEach(function each(client) {
    //     console.log(client.userName);
    // });

    ws.on('message', function incoming(message) {
        var clientData = JSON.parse(message);
        console.log('received: %s %s', ws.userName, JSON.stringify(clientData));
        var debugData = {
            type: 'debug',
            data: {
                debugData: clientData
            }
        };

        ws.send(JSON.stringify(debugData));

        var type = clientData.type;
        var data = clientData.data;

        switch (type) {
            case 'login':
                var userName = data.userName;

                if (userNotExists(userName)) {
                    ws.userName = userName;
                    var data = {
                        logged: true,
                        message: 'Prijava uspešna'
                    }
                    var output = makeOutputData('login', data);
                    ws.send(JSON.stringify(output));

                    //vsem clientom pošljemo da se je prijavil nov uporabnik
                    var usersData = {
                        users: getActiveUsers()
                    }
                    var outputUsers = makeOutputData('getUsers', usersData);
                    wss.broadcast(JSON.stringify(outputUsers));
                } else {
                    var data = {
                        logged: false,
                        message: 'Izberi si drugo uporabniško ime'
                    };
                    var output = makeOutputData('login', data);
                    ws.send(JSON.stringify(output));
                }
                break;
            case 'chat':
                var receiver = data.receiver;
                var message = data.message;
                wss.clients.forEach(function each(client) {
                    if (client.userName == receiver) {
                        var data = {
                            sender: ws.userName,
                            message: message
                        };
                        var output = makeOutputData('chat', data);
                        client.send(JSON.stringify(output));
                    }
                });
                break;
            case 'publicKeyRequest':
                var receiver = data.receiver;
                wss.clients.forEach(function each(client) {
                    if (client.userName == receiver) {
                        var data = {
                            sender: ws.userName
                        };
                        var output = makeOutputData('publicKeyRequest', data);
                        client.send(JSON.stringify(output));
                    }
                });
                break;
            case 'sendPublicKey':
                var publicKey = data.publicKey;
                var receiver = data.receiver;
                wss.clients.forEach(function each(client) {
                    if (client.userName == receiver) {
                        var data = {
                            sender: ws.userName,
                            publicKey: publicKey
                        };
                        var output = makeOutputData('sendPublicKey', data);
                        client.send(JSON.stringify(output));
                    }
                });
                break;
            case 'getUsers':
                var data = {
                    users: getActiveUsers()
                }
                var output = makeOutputData('getUsers', data);
                ws.send(JSON.stringify(output));
                break;
        }
    });

    ws.on('close', function() {
        console.log('client closed');
        //vsem clientom pošljemo da se je odjavil uporabnik
        var usersData = {
            users: getActiveUsers()
        }
        var outputUsers = makeOutputData('getUsers', usersData);
        wss.broadcast(JSON.stringify(outputUsers));
    });

    ws.send(JSON.stringify({
        type: 'connected',
        data: {
            message: 'Povezan s strežnikom'
        }
    }));
});

function userNotExists(userNameToCheck) {
    userNameToCheck = userNameToCheck.trim();
    var existingUsers = getActiveUsers();
    return existingUsers.indexOf(userNameToCheck) == -1;
}

function getActiveUsers() {
    var usersArray = [];
    wss.clients.forEach(function each(client) {
        usersArray.push(client.userName);
    });
    return usersArray;
}

function makeOutputData(type, data) {
    var output = {
        type: type,
        data: data
    };
    return output;
}

wss.broadcast = function(data) {
    wss.clients.forEach(function each(client) {
        try {
            client.send(data);
        } catch (err) {
            console.log(err);
        }
    });
};

WSserver.on('request', app);
WSserver.listen(port, function() {
    console.log('Listening on ' + WSserver.address().port);
    setInterval(startGenerationKeyRequest, 1000 * 60 * 30); //generiranje novega ključa vsakih 30 minut
});

function startGenerationKeyRequest() {
    var outputUsers = makeOutputData('generateNewKey', {});
    wss.broadcast(JSON.stringify(outputUsers));
}

module.exports = app;